import React from "react";
import {
  StyleSheet,
  StatusBar,
  LogBox,
  ImageBackground,
  KeyboardAvoidingView,
  Platform,
  Dimensions,
} from "react-native";
import Constants from "expo-constants";
import { Routes } from "./src/routes/index";
import { AuthProvider } from "./src/hooks/index";

// Ignora os logs no ambiente de desenvolvimento
LogBox.ignoreAllLogs();

export default function App() {
  return (
    // Imagem de plano de fundo, presente em toda a aplicação
    <ImageBackground
      source={require("./assets/background.png")}
      style={styles.background}
    >
      <KeyboardAvoidingView
        // Especifica como deve reagir à presença do teclado. Se for IOS, ele dá um padding. Se for Android, nada
        behavior={Platform.OS === "ios" ? "padding" : undefined}
        style={styles.keyboard}
      >
        {/* Barra de notificações transparente */}
        <StatusBar style="auto" translucent backgroundColor={"transparent"} />
        {/* Autenticação */}
        <AuthProvider>
          {/* Rotas */}
          <Routes />
        </AuthProvider>
      </KeyboardAvoidingView>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  keyboard: {
    flex: 1,
    paddingTop: Constants.statusBarHeight,
    width: Dimensions.get("screen").width,
  },
  background: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
    alignItems: "center",
  },
});
