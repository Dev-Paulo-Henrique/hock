//Exportação de uma variável com conjunto de cores
export const theme = {
  colors: {
    purple: "#54379E",
    white: "#FFFFFF",
    black: "#000000",
    placeholder: "#8E8E8E",
    cancel: "#FC4D1E",
    green: "#44FC5B",
    orange: "#FD942A",
    button: "#EEEEEE",
    lightBlue: "#1F7ADE",
    blueGradient: {
      0: "#1E5ADE",
      100: "#3535C1",
    },
    whiteGradient: {
      0: "#FFFFFF",
      100: "#DCDCDC",
    },
  },
};
