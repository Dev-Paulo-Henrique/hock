import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { useAuth } from "../hooks/index";
import { Login } from "../../pages/Login";
import { Home } from "../../pages/Home";
import { News } from "../../pages/News";
import { Chat } from "../../pages/Chat";
import { Dashboard } from "../../pages/Dashboard";
import { Profile } from "../../pages/Profile";
import { theme } from "../styles";
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem
} from "@react-navigation/drawer";
import { Header } from "../components/Header";

// Uso de constantes da função de criação do sidebar
const { Navigator, Screen } = createDrawerNavigator();

// Criação do Sidebar
function CustomDrawerContent(props) {
  const { setIsLogged } = useAuth()
  return (
    <DrawerContentScrollView {...props}>
      {/* Nome e foto do usuário, atributo "click" é o "X" para fechar o sidebar */}
      <Header click={() => props.navigation.closeDrawer()} />
      {/* Listagem das páginas */}
      <DrawerItemList {...props} />
      {/* Logout */}
      <DrawerItem label="Sair" onPress={() => setIsLogged(false)} labelStyle={{color: theme.colors.cancel, fontWeight: "bold"}}/>
    </DrawerContentScrollView>
  );
}

// Exportação das rotas
export function Routes() {
  // Resgata o boolean usado na função de autenticação
  const { isLogged } = useAuth();

  // Condicional para trocar a página de login pela de rotas, caso o usuário estiver autenticado
  return (
    <NavigationContainer>
      {!isLogged ? (
        <Login />
      ) : (
        <Navigator
          // Nome da página inicial
          initialRouteName="Home"
          screenOptions={{
            // Sombra do Header
            headerShadowVisible: false,
            // Nome do Header, se não houver, ele usa o nome das páginas como título
            headerTitle: "HOCK",
            // Cor de fundo
            headerStyle: {
              backgroundColor: "transparent",
            },
            // Cor do conteúdo do Header
            headerTintColor: theme.colors.white,
            // Background "transparent" para mostrar a imagem de background da aplicação
            sceneContainerStyle: {
              backgroundColor: "transparent",
            },
          }}
          // Reanimação(Documentação)
          useLegacyImplementation
          // Listagem das páginas, para a navegação
          drawerContent={(props) => <CustomDrawerContent {...props} />}
        >
          {/* Páginas */}
          <Screen name="Home" component={Home} />
          <Screen name="Notícias" component={News} />
          <Screen name="Perfil" component={Profile} />
          <Screen name="Dashboard" component={Dashboard} />
          <Screen
            name="Chat"
            component={Chat}
            // Não uso Header pois é a página de chat, e ele tem botão de voltar para a página Home
            options={{ headerShown: false }}
          />
        </Navigator>
      )}
    </NavigationContainer>
  );
}
