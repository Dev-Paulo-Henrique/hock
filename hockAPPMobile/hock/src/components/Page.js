import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Arrow } from "./Arrow";
import { theme } from "../styles/index";

export function Page(props) {
  return (
    <View style={styles.container}>
      {/* Botão de voltar */}
      <Arrow click={props.leftButton} name="chevron-left" />
      {/* Nome da página */}
      <Text style={styles.text}>{props.name}</Text>
      {/* Botão de avançar */}
      <Arrow click={props.rightButton} name="chevron-right" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginHorizontal: 40,
    marginBottom: 40,
  },
  text: {
    color: theme.colors.white,
    fontWeight: "bold",
    fontSize: 30,
  },
});
