import React from "react";
import { Text, StyleSheet, View, Dimensions } from "react-native";
import { theme } from "../styles";
import { LinearGradient } from "expo-linear-gradient";

export function Messages(props) {
  return (
    <LinearGradient
      colors={
        props.person === "Professor"
          ? [theme.colors.blueGradient[0], theme.colors.blueGradient[100]]
          : [theme.colors.whiteGradient[0], theme.colors.whiteGradient[100]]
      }
      style={[
        styles.gradient,
        props.person === "Professor" && {
          alignSelf: "flex-end",
        },
      ]}
    >
      <Text
        {...props.person}
        style={[
          styles.text,
          props.person === "Professor" && { color: theme.colors.white },
        ]}
      >
        {props.children}
      </Text>
      <View>
        <Text
          {...props.time}
          style={[
            styles.time,
            props.person === "Professor" && {
              color: theme.colors.whiteGradient[100],
            },
          ]}
        >
          {props.time}
          {/* 12:35 */}
        </Text>
      </View>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  text: {
    color: theme.colors.purple,
    fontSize: 20,
  },
  time: {
    color: theme.colors.placeholder,
    fontSize: 12,
    alignSelf: "flex-end",
    marginBottom: -10,
    marginRight: -8,
  },
  gradient: {
    padding: 15,
    borderRadius: 10,
    maxWidth: Dimensions.get("screen").width / 1.3,
    alignSelf: "flex-start",
    alignItems: "flex-end",
    flexDirection: "row",
    marginVertical: 5,
    marginHorizontal: 10,
  },
});
