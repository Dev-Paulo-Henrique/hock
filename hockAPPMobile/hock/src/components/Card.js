import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import { theme } from "../styles/index";

export function Card(props) {
  return (
    <View style={styles.card}>
      <View style={styles.content}>
        {/* Foto de perfil */}
        <Image
          source={{
            uri: `${props.image}`,
          }}
          style={styles.image}
        />
        <View style={styles.data}>
          {/* Nome do usuário */}
          <Text
            numberOfLines={1}
            style={[
              styles.text,
              {
                fontWeight: "bold",
                fontSize: 32,
                color: theme.colors.blueGradient[0],
                opacity: 1,
              },
            ]}
          >
            {props.name}
          </Text>
          {/* Cargo */}
          <Text style={styles.text}>{props.function}</Text>
          {/* Email */}
          <Text style={styles.text}>{props.email}</Text>
          {/* Telefone */}
          <Text style={styles.text}>{props.phone}</Text>
        </View>
      </View>
      {/* Botão para acessar os dados/ ir para a página de atualização */}
      <TouchableOpacity
        style={styles.button}
        onPress={() => console.log("Meus Dados")}
      >
        <Text
          style={[
            styles.text,
            {
              color: theme.colors.orange,
              fontWeight: "bold",
              opacity: 1,
            },
          ]}
        >
          Meus Dados
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  image: {
    marginLeft: -5,
    width: 80,
    height: 80,
    borderRadius: 10,
  },

  text: {
    color: theme.colors.blueGradient[0],
    opacity: 0.5,
    alignSelf: "flex-end",
    fontSize: 12,
  },

  card: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 10,
    marginVertical: 10,
    // marginTop: 40,
    width: Dimensions.get("screen").width / 1.2,
    padding: 10,
    height: 160,
    borderRadius: 10,
    backgroundColor: theme.colors.white,
    alignSelf: "center",
    shadowColor: theme.colors.black,
    shadowOffset: { width: 5, height: 5 },
    shadowOpacity: 1,
    shadowRadius: 10,
    elevation: 10,
  },

  content: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: Dimensions.get("screen").width / 1.2,
    borderRadius: 10,
  },

  button: {
    paddingVertical: 10,
    paddingHorizontal: 25,
    backgroundColor: theme.colors.button,
    borderRadius: 30,
    marginTop: 20,
  },

  data: {
    marginLeft: 20,
    maxWidth: 220,
  },
});
