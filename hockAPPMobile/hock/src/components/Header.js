import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { theme } from "../styles/index";
import { Entypo } from "@expo/vector-icons";

export function Header(props) {
  return (
    <View style={styles.container}>
      <View style={styles.container}>
        {/* Foto do usuário */}
        <Image
          style={styles.image}
          source={{
            uri: "https://avatars.githubusercontent.com/u/95699609?v=4",
          }}
        />
        {/* Nome do usuário */}
        <Text numberOfLines={1} style={styles.textLogo}>
          Bruno Vitor
        </Text>
      </View>
      {/* Botão de fechaar Sidebar */}
      <TouchableOpacity onPress={props.click} style={styles.close}>
        <Entypo name="cross" size={24} color={theme.colors.white} />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  image: {
    marginVertical: 10,
    marginRight: 10,
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginHorizontal: 10,
  },
  textLogo: {
    color: theme.colors.black,
    fontWeight: "bold",
    fontSize: 22,
    // maxWidth: Dimensions.get("screen").width - 80
  },
  close: {
    flexDirection: "row",
    alignItems: "center",
    // alignSelf: "center",
    backgroundColor: theme.colors.cancel,
    padding: 10,
    borderRadius: 10,
    // marginTop: 50
  },
  textClose: {
    fontWeight: "bold",
    color: theme.colors.white,
  },
});
