import React from "react";
import { StyleSheet } from "react-native";
import { WebView } from "react-native-webview";

export function Iframe() {
  return (
    <WebView
      style={styles.container}
      // A lista de strings permitem curingas e são comparadas apenas com a origem
      originWhitelist={["*"]}
      // Gerar HTML estático
      source={{
        html: "<iframe width='700' height='500' src='https://maps.google.com/maps?q=funda%C3%A7%C3%A3o%20bradesco&t=&z=13&ie=UTF8&iwloc=&output=embed' title='description'></iframe>",
      }}
    />
  );
}

const styles = StyleSheet.create({
  container: {
    height: 415,
    width: 800,
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: 10,
    alignSelf: "center",
  },
});
