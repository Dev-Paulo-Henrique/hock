import React from "react";
import { StyleSheet, Dimensions, TextInput, View, Text } from "react-native";
import { theme } from "../styles";

export function Input(props) {
  return (
    <TextInput
      secureTextEntry={props.password}
      style={styles.input}
      placeholder={props.placeholder}
      keyboardType={props.type}
      placeholderTextColor={theme.colors.placeholder}
      selectionColor={theme.colors.orange}
      onChangeText={props.onChangeText}
      value={props.value}
      autoCapitalize={props.autoCapitalize}
    />
  );
}

export function InputLogin(props) {
  return (
    <View style={[styles.container, props.error && { borderColor: theme.colors.cancel, borderWidth: 1 }]}>
      <Text style={styles.label}>{props.label}</Text>
      <TextInput
        secureTextEntry={props.password}
        style={styles.inputLogin}
        placeholder={props.placeholder}
        keyboardType={props.type}
        placeholderTextColor={theme.colors.placeholder}
        selectionColor={theme.colors.orange}
        onChangeText={props.onChangeText}
        onBlur={props.onBlur}
        value={props.value}
        autoCapitalize={props.autoCapitalize} 
      />
      {props.children}
    </View>
  );
}

const styles = StyleSheet.create({
  input: {
    height: 40,
    width: Dimensions.get("screen").width / 1.2,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: theme.colors.white,
    borderRadius: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
    alignSelf: "center",
  },
  inputLogin: {
    height: 100,
    width: 250,
    flex: 1,
    marginHorizontal: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  label: {
    fontWeight: "bold",
    height: "100%",
    textAlignVertical: "center",
    fontSize: 16,
    padding: 10,
  },
  container: {
    backgroundColor: theme.colors.white,
    borderRadius: 25,
    height: 40,
    width: Dimensions.get("screen").width / 1.5,
    marginBottom: 40,
    // marginTop: 20,
    alignItems: "center",
    flexDirection: "row",
    marginHorizontal: 50,
  },
});
