import React from "react";
import { TouchableOpacity } from "react-native";
import { theme } from "../styles/index";
import { Entypo } from "@expo/vector-icons";

export function Arrow(props) {
  return (
    // Botão das setas para voltar ou avançar uma página
    <TouchableOpacity onPress={props.click}>
      {/* Ícone */}
      <Entypo name={props.name} size={50} color={theme.colors.orange} />
    </TouchableOpacity>
  );
}
