import React from "react";
import { Text, StyleSheet, View, Dimensions, Image } from "react-native";
import { theme } from "../../styles";
import { LinearGradient } from "expo-linear-gradient";

export function NewsCard(props) {
  return (
    // Balão de fala(Gradiente)
    <LinearGradient
      colors={
        props.type === "Professor"
          ? [theme.colors.blueGradient[0], theme.colors.blueGradient[100]]
          : [theme.colors.whiteGradient[0], theme.colors.whiteGradient[100]]
      }
      style={[
        styles.gradient,
        props.type === "Professor" && {
          alignSelf: "flex-end",
        },
      ]}
    >
      {/* <Image
        source={{
          uri: `https://agenciadenoticias.ibge.gov.br/${
            props.image
          }`,
        }}
        style={{ width: 200, height: 200 }}
      /> */}

      {/* Mensagem */}
      <Text
        {...props.type}
        style={[
          styles.text,
          props.type === "Professor" && { color: theme.colors.white },
        ]}
      >
        {props.children}
      </Text>
      <View>
        {/* Horário que a mensagem foi enviada */}
        <Text
          {...props.time}
          style={[
            styles.time,
            props.type === "Professor" && {
              color: theme.colors.whiteGradient[100],
            },
          ]}
        >
          {props.time}
          {/* 12:35 */}
        </Text>
      </View>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  text: {
    color: theme.colors.purple,
    fontSize: 20,
  },
  time: {
    color: theme.colors.placeholder,
    fontSize: 12,
    alignSelf: "flex-end",
    marginBottom: -10,
    marginRight: -8,
  },
  gradient: {
    padding: 15,
    borderRadius: 10,
    width: Dimensions.get("screen").width / 1.1,
    alignSelf: "flex-start",
    alignItems: "flex-end",
    flexDirection: "row",
    marginVertical: 5,
    marginHorizontal: 10,
  },
});
