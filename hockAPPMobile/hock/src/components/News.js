import React, { useState, useEffect } from "react";
import axios from "axios";
import { news } from "../../services/api";
import { View, Text, FlatList, Image } from "react-native";
import { NewsCard } from "../NewsCard/index";

export function News() {
  const [getNews, setGetNews] = useState([]);

    useEffect(() => {
    async function loadNews() {
      const response = await news.get(
        "api/v3/noticias/?busca=home&qtd=5&tipo=noticia"
      );

      setGetNews(response.data.items);
    }

    loadNews();
  }, [getNews]);
  
  const DATA = getNews.map((news) => [
    news?.titulo,
    news?.introducao,
    news?.id,
    news?.data_publicacao,
    news?.imagens,
  ]);

  return (
    <FlatList
      style={{ alignSelf: "center", flex: 1, marginTop: -470 }}
      data={DATA}
      keyExtractor={(DATA) => DATA?.id}
      renderItem={({ item }) => (
        <>
          {/* {console.log(JSON.parse(item[4]).image_fulltext)} */}
          <NewsCard key={item[2]} image={JSON.parse(item[4]).image_intro}>
            {item[0]}
          </NewsCard>
        </>
      )}
    />
  );
}
