import React, { createContext, useContext, useState } from "react";
import { Alert } from "react-native";

//Criação de um contexto vazio para ser utilizado em toda a aplicação
export const AuthContext = createContext({});

//Componente que engloba as rotas da aplicação
function AuthProvider({ children }) {
  //Criação de uma variável boolean.
  //Para acessar as rotas sem passar pela autenticação(Página de login), use "true" na constante abaixo
  const [isLogged, setIsLogged] = useState(false);

  //Função para fazer a validação dos dados: email e senha, recebidos da página de Login(Coverage)
  async function signIn(email, password) {
    if (!email || !password) {
      //Ele retorna esse alerta, se um dos dois campos estiverem vazios
      return Alert.alert("Formulário vazio", "Preencha os campos");
    }
    if (email !== "brunovpn123456@gmail.com" || password !== "12345678") {
      //Ele retorna esse alerta, se os dois campos estiverem diferentes da condição acima
      return Alert.alert("Usuário não encontrado", "Dados incorretos");
    }

    //Se o usuário estiver cadastrado no banco de dados, ele pode acessar as rotas
    setIsLogged(true);
  }

  return (
    <AuthContext.Provider
      //Exporta a função(SignIn) para fazer a validação e a verificação(isLogged) do usuário para acessar as rotas
      value={{
        signIn,
        isLogged,
        setIsLogged
      }}
    >
      {/* Children é as rotas que são usadas no arquivo App.js */}
      {children}
    </AuthContext.Provider>
  );
}

//Contexto que usa a função SignIn da função criada acima
function useAuth() {
  const context = useContext(AuthContext);

  return context;
}

export { AuthProvider, useAuth };
