import axios from "axios";

// Exportação das API's para serem usadas na aplicação
// Chat, feito no CodeSandBox e publicada online pelo mesmo
export const api = axios.create({
  baseURL: "https://uq9i08.sse.codesandbox.io",
});

// API de Notícias do IBGE
export const news = axios.create({
  baseURL: "https://servicodados.ibge.gov.br/",
});
