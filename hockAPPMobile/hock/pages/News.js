import React from "react";
import { StyleSheet, View } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { Page } from "../src/components/Page";

export function News() {
  // Navegação das telas
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      {/* Nome da página + setas de voltar e avançar página */}
      <Page
        name="Notícias"
        leftButton={() => navigation.navigate("Home")}
        rightButton={() => navigation.navigate("Perfil")}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
