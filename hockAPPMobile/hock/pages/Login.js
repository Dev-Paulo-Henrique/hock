import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import { theme } from "../src/styles/index";
import { useAuth } from "../src/hooks/index";
import { Entypo } from "@expo/vector-icons";
import { InputLogin } from "../src/components/Input";
import { Formik } from "formik";
import * as yup from "yup";

export function Login() {
  // Email do login
  // const [email, setEmail] = useState("");
  // Senha do login
  // const [password, setPassword] = useState("");
  // Booleano para mostrar a senha que o usuário digitou
  const [showPassword, setShowPassword] = useState(false);
  // Booleano para mostrar que está carregando a validação(Deixei assim por enquanto, pois estou sem API)
  const [loading, setLoading] = useState(false);
  // Função para fazer a autenticação
  const { signIn } = useAuth();

  const schema = yup.object().shape({
    email: yup
      .string()
      .email("Insira um email válido.")
      .required("Email obrigatório."),
    password: yup
      .string()
      .min(6, "Deve conter no mínimo 6 caracteres.")
      .required("Senha obrigatória."),
  });

  return (
    <View style={styles.container}>
      {/* Logo */}
      <Image style={styles.image} source={require("../assets/icon.png")} />
      <Formik
        initialValues={{ email: "", password: "" }}
        validateOnMount={true}
        onSubmit={(values) => console.log(values)}
        validationSchema={schema}
      >
        {({
          handleChange,
          handleBlur,
          handleSubmit,
          values,
          touched,
          isValid,
          errors,
        }) => (
          <>
            {/* Email */}
            <InputLogin
              password={false}
              placeholder="someone@example.com"
              type="email-address"
              label="Email"
              autoCapitalize="none"
              onChangeText={handleChange("email")}
              onBlur={handleBlur("email")}
              value={values.email}
              error={errors.email && touched.email}
            />
            {errors.email && touched.email && <Text style={styles.error}>{errors.email}</Text>}
            {/* Senha */}
            <InputLogin
              password={!showPassword}
              placeholder="••••••••"
              autoCapitalize="none"
              label="Senha"
              onChangeText={handleChange("password")}
              onBlur={handleBlur("password")}
              value={values.password}
              error={errors.password && touched.password}
              children={
                // Botão para mostrar senha
                <TouchableOpacity
                  style={styles.showPassword}
                  onPress={() =>
                    showPassword
                      ? setShowPassword(false)
                      : setShowPassword(true)
                  }
                >
                  {/* Troca dos ícones */}
                  {showPassword ? (
                    <Entypo
                      name="eye"
                      size={24}
                      color={theme.colors.placeholder}
                    />
                  ) : (
                    <Entypo
                      name="eye-with-line"
                      size={24}
                      color={theme.colors.placeholder}
                    />
                  )}
                </TouchableOpacity>
              }
            />
            {errors.password && touched.password && (
              <Text style={styles.error}>{errors.password}</Text>
            )}

            {/* Botão de Submit */}
            <TouchableOpacity
              disabled={!isValid}
              style={[styles.loginBtn, !isValid && { backgroundColor: theme.colors.placeholder }]}
              onPress={async () => {
                await signIn(values.email, values.password), console.log(values);
                setLoading(true), setTimeout(() => setLoading(false), 2000);
              }}
            >
              {/* Troca da palavra "Login" pelo carregamento, quando for clicado */}
              {!loading ? (
                <Text style={styles.TextInputbtn}>Login</Text>
              ) : (
                <ActivityIndicator color={theme.colors.white} />
              )}
            </TouchableOpacity>
          </>
        )}
      </Formik>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  image: {
    marginBottom: 50,
    width: 200,
    height: 200,
  },
  showPassword: {
    paddingRight: 10,
  },
  TextInputbtn: {
    color: theme.colors.white,
    fontWeight: "bold",
  },
  loginBtn: {
    width: 150,
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    backgroundColor: theme.colors.orange,
    // marginTop: 40
  },
  error: {
    color: theme.colors.cancel,
    fontWeight: "bold",
    marginTop: -40,
    marginBottom: 20
  }
});
