import React, { useState, useCallback } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Dimensions,
} from "react-native";
import { theme } from "../src/styles";
import { Input } from "../src/components/Input";
import { Card } from "../src/components/Card";
import { FancyAlert } from "react-native-expo-fancy-alerts";
import { useNavigation } from "@react-navigation/native";
import { Page } from "../src/components/Page";

export function Profile() {
  // Navegação das telas
  const navigation = useNavigation();
  // Nome do usuário
  const [name, setName] = useState("Bruno V. P. N.");
  // Cargo do usuário
  const [functions, setFunctions] = useState("Professor e Gerente de Projetos");
  // Email do usuário
  const [email, setEmail] = useState("brunovpn123456@gmail.com");
  // Telefone do usuário
  const [phone, setPhone] = useState("+55 81 3456-0000");
  // Senha do usuário
  const [password, setPassword] = useState("12345678");
  // Booleano para alert(Desnecessário)
  const [visible, setVisible] = useState(false);
  // Função para mostrar o alert
  const toggleAlert = useCallback(() => {
    setVisible(!visible);
  }, [visible]);

  // Função para atualizar dados
  async function handleUpdate() {
    console.log(name, email, phone, functions, password);
    setName("");
    setEmail("");
    setPhone("");
    setPassword("");
    setFunctions("");
  }

  return (
    <ScrollView>
      {/* Nome da página + setas de voltar e avançar página */}
      <Page
        name="Perfil"
        leftButton={() => navigation.navigate("Notícias")}
        rightButton={() => navigation.navigate("Dashboard")}
      />
      {/* Componente criado semelhante à um cartão */}
      <Card
        image="https://avatars.githubusercontent.com/u/95699609?v=4"
        name={name}
        function={functions}
        email={email}
        phone={phone}
      />
      {/* Input de Nome */}
      <Input
        placeholder="Nome Completo"
        password={false}
        onChangeText={(value) => setName(value)}
        value={name}
        autoCapitalize="words"
      />
      {/* Input de Email */}
      <Input
        placeholder="Email"
        password={false}
        onChangeText={(value) => setEmail(value)}
        value={email}
        type="email-address"
        autoCapitalize="none"
      />
      {/* Input de Cargo */}
      <Input
        placeholder="Cargo"
        password={false}
        onChangeText={(value) => setFunctions(value)}
        value={functions}
        autoCapitalize="words"
      />
      {/* Input de Telefone */}
      <Input
        placeholder="Telefone"
        password={false}
        onChangeText={(value) => setPhone(value)}
        value={phone}
        type="phone-pad"
      />
      {/* Input de Senha */}
      <Input
        placeholder="Senha"
        password={true}
        onChangeText={(value) => setPassword(value)}
        value={password}
        autoCapitalize="none"
      />
      {/* Botões */}
      <View
        style={{
          flexDirection: "row",
          marginTop: 20,
          alignItems: "center",
          justifyContent: "space-between",
          alignSelf: "center",
        }}
      >
      {/* Botão de atualizar */}
        <TouchableOpacity
          style={styles.choosebutton}
          onPress={() => [handleUpdate().finally(toggleAlert)]}
        >
          <Text style={styles.textButton}>Salvar</Text>
        </TouchableOpacity>
      {/* Botão de cancelar */}
        <TouchableOpacity
          onPress={() => {}}
          style={[
            styles.choosebutton,
            { backgroundColor: theme.colors.cancel },
          ]}
        >
          <Text style={styles.textButton}>Cancelar</Text>
        </TouchableOpacity>
      </View>
      {/* Alert(Desnecessário) */}
      <FancyAlert
        visible={visible}
        icon={
          <View
            style={{
              flex: 1,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: theme.colors.green,
              borderRadius: 50,
              width: "100%",
            }}
          >
            <Text style={{ fontSize: 30 }}>✅</Text>
          </View>
        }
        style={{ backgroundColor: "white" }}
      >
        <Text
          style={{
            marginTop: -16,
            marginBottom: 32,
            fontSize: 22,
            fontWeight: "bold",
          }}
        >
          Alteração realizada
        </Text>
        <TouchableOpacity style={styles.closeButton} onPress={toggleAlert}>
          <Text style={styles.textButton}>Fechar</Text>
        </TouchableOpacity>
      </FancyAlert>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  textButton: {
    color: theme.colors.white,
    fontWeight: "bold",
    fontSize: 20,
    marginHorizontal: 10,
  },

  choosebutton: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 10,
    marginVertical: 5,
    paddingHorizontal: 10,
    height: 60,
    width: 120,
    borderRadius: 10,
    backgroundColor: theme.colors.green,
  },

  closeButton: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 10,
    marginVertical: 10,
    marginTop: -25,
    padding: 10,
    borderRadius: 10,
    backgroundColor: theme.colors.cancel,
  },

  image: {
    marginLeft: -5,
    width: 80,
    height: 80,
    borderRadius: 10,
  },

  text: {
    color: theme.colors.blueGradient[0],
    opacity: 0.5,
    alignSelf: "flex-end",
    fontSize: 12,
  },

  card: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 10,
    marginVertical: 10,
    // marginTop: 40,
    width: Dimensions.get("screen").width / 1.2,
    padding: 10,
    height: 160,
    borderRadius: 10,
    backgroundColor: theme.colors.white,
    alignSelf: "center",
    shadowColor: theme.colors.black,
    shadowOffset: { width: 5, height: 5 },
    shadowOpacity: 1,
    shadowRadius: 10,
    elevation: 10,
  },

  content: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: Dimensions.get("screen").width / 1.2,
    borderRadius: 10,
  },

  button: {
    paddingVertical: 10,
    paddingHorizontal: 25,
    backgroundColor: theme.colors.button,
    borderRadius: 30,
    marginTop: 20,
  },

  data: {
    marginLeft: 20,
    maxWidth: 220,
  },
});
