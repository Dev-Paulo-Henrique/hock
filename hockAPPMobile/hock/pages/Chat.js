import React, { useState, useEffect, useRef } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  TextInput,
  ActivityIndicator,
} from "react-native";
import { Entypo } from "@expo/vector-icons";
import { theme } from "../src/styles";
import { Messages } from "../src/components/Messages";
import UserAvatar from "@muhzi/react-native-user-avatar";
import { api } from "../src/services/api";
import moment from "moment";
import { useNavigation } from "@react-navigation/native";

export function Chat() {
  // Navegação das telas
  const navigation = useNavigation();
  // Listagem das mensagens
  const [messages, setMessages] = useState([]);
  // Texto do input
  const [newMessage, setNewMessage] = useState("");
  // Renderizar scrollView
  const scrollViewRef = useRef();

  // Carregar as mensagens
  useEffect(() => {
    async function loadMessages() {
      const response = await api.get("/message");

      // Colocar as mensagens num array
      setMessages(response.data);
    }

    loadMessages();
  }, [messages]);

  // Mapear as mensagens
  const DATA = messages.map((message) => [
    message?.name,
    message?.message,
    message?.time,
    message?._id,
  ]);

  // Enviar uma mensagem
  async function handleAddMessage() {
    if (newMessage) {
      // Input em branco
      setNewMessage("");
      // Abaixo você pode trocar "Professor" por "Aluno" para modificar "Web" para "Mobile"
      const response = await api.post("/message", {
        message: newMessage,
        name: "Professor",
        // name: "Aluno",
        time: moment().format("HH:mm"),
      });

      // Retorna todas as outras mensagens do banco de dados + mensagens recentes
      setMessages([...messages, response.data]);
    }
    return;
  }

  return (
    <>
      {/* Parte superior do Chat */}
      <View style={styles.user}>
        {/* Botão para voltar para a página "Home" */}
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={styles.back}
        >
          {/* ícone */}
          <Entypo
            name="chevron-left"
            size={30}
            color={theme.colors.blueGradient[0]}
          />
        </TouchableOpacity>
        {/* Avatar do usuário. Se não houver "src", ele usa a inicial do nome do usuário como perfil */}
        <UserAvatar
          // Nome do usuário
          userName={"Paulo Henrique"}
          // Tamanho do avatar
          size={40}
          // Cor de fundo para ser usado com a inicial do nome
          backgroundColor={theme.colors.purple}
          // Imagem de perfil
          src="https://avatars.githubusercontent.com/u/67799522?v=4"
          // Online
          active={true}
          // Arredondado
          rounded
        />
        {/* Nome do usuário */}
        <Text style={styles.userName}>Paulo Henrique</Text>
      </View>

      {/* Parte da visualização das mensagens */}
      {messages.length !== 0 ? (
        <ScrollView
          style={styles.container}
          // Não mostra a barra do Scroll
          showsVerticalScrollIndicator={false}
          // Recebe a função useRef()
          ref={scrollViewRef}
          // Sempre vai para o final do Scroll com animação, toda vez que uma mensagem for enviada
          onContentSizeChange={() =>
            scrollViewRef.current.scrollToEnd({ animated: true })
          }
        >
          {/* Listagem das mensagens */}
          {DATA.map((item) => {
            return (
              <Messages key={item[3]} time={item[2]} person={item[0]}>
                {item[1]}
              </Messages>
            );
          })}
        </ScrollView>
      ) : (
        // Componente que mostra que as mensagens estão carregando
        <ActivityIndicator
          color={theme.colors.orange}
          size={50}
          style={{ flex: 1 }}
        />
      )}

      {/* Parte inferior do Chat*/}
      <View style={styles.input}>
        {/* Input */}
        <TextInput
          style={styles.textInput}
          placeholder="Digite algo..."
          value={newMessage}
          placeholderTextColor={theme.colors.placeholder}
          selectionColor={theme.colors.blueGradient[0]}
          onChangeText={(value) => setNewMessage(value)}
        />
        {/* Botão para enviar a mensagem */}
        <TouchableOpacity style={styles.button} onPress={handleAddMessage}>
          <Entypo
            name="paper-plane"
            size={30}
            color={theme.colors.blueGradient[0]}
          />
        </TouchableOpacity>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: Dimensions.get("screen").width,
  },
  user: {
    backgroundColor: theme.colors.white,
    paddingVertical: 10,
    paddingHorizontal: 10,
    width: Dimensions.get("screen").width,
    flexDirection: "row",
    alignItems: "center",
  },
  userName: {
    color: theme.colors.blueGradient[0],
    fontWeight: "bold",
    fontSize: 26,
    marginLeft: 10,
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 25,
    marginHorizontal: 10,
  },

  back: {
    marginRight: 10,
  },
  textInput: {
    paddingLeft: 10,
    alignItems: "center",
    justifyContent: "center",
    width: Dimensions.get("screen").width / 1.15,
    height: 50,
  },
  input: {
    backgroundColor: theme.colors.white,
    width: Dimensions.get("screen").width,
    flexDirection: "row",
    alignItems: "center",
  },
  button: {
    padding: 10,
  },
});
