import React from "react";
import { StyleSheet, View, ScrollView } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { Iframe } from "../src/components/Iframe";
import { Page } from "../src/components/Page";

export function Home() {
  // Navegação das telas
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      {/* Nome da página + setas de voltar e avançar página */}
      <Page
        name="Home"
        leftButton={() => navigation.navigate("Dashboard")}
        rightButton={() => navigation.navigate("Notícias")}
      />
      {/* Carregar o Iframe(Não sei o que vai ser colocado exatamente, por isso o scroll) */}
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{ marginTop: -20 }}
      >
        {/* Componente criado em HTML */}
        <Iframe />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
